package com.zuitt.example;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Arrays;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNum = new int[5];

        primeNum[0] = 2;
        primeNum[1] = 3;
        primeNum[2] = 5;
        primeNum[3] = 7;
        primeNum[4] = 11;

        Scanner input = new Scanner(System.in);

        System.out.print("Enter index number to get the corresponding prime number:");
        int num = input.nextInt();

        switch (num) {
            case 1:
                System.out.println(("The first prime number is: " + primeNum[num-1]));
                break;
            case 2:
                System.out.println(("The second prime number is: " + primeNum[num-1]));
                break;
            case 3:
                System.out.println(("The third prime number is: " + primeNum[num-1]));
                break;
            case 4:
                System.out.println(("The fourth prime number is: " + primeNum[num-1]));
                break;
            case 5:
                System.out.println(("The fifth prime number is: " + primeNum[num-1]));
                break;
            default:
                System.out.println("Invalid input. Please select from 1 to 5");
        }

        ArrayList<String> myFriends = new ArrayList<>(Arrays.asList("Este", "Betty", "Inez", "Marjorie"));
        System.out.println("My friends are: " + myFriends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory is consist of : " + inventory);

    }


}
