package com.zuitt.example;
import java.util.Scanner;
public class LeapYear {

    public static void main(String[] args) {

        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Input a year to be checked if a leap year");

        int checkYear = yearScanner.nextInt();

        if ( (checkYear % 4 == 0) && ((checkYear % 100 != 0) || checkYear % 400 == 0)) {
            System.out.println(checkYear + " is a leap year!");
        }
        else {
            System.out.println(checkYear + " is not a leap year!");
        }
    }
}
